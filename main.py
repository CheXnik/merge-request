import json
import requests

from flask import Flask, request, Response
from utils import gen_password, parse_length, execute_query

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/now')
def get_current_time():
    return str(get_current_time())


@app.route('/password')
def get_random():
    default = '0'
    charss = request.args.get('chars', str(default))
    try:
        length = parse_length(request, 10)
    except Exception as ex:
        return Response(str(ex), status=400)
    return gen_password(length, charss)


@app.route('/bitcoin')
def get_bitcoin_rate():
    default = "USD"
    currency = request.args.get('currency', str(default))
    response = requests.get(
        url='https://bitpay.com/api/rates'
    )
    rates = json.loads(response.text)
    for rate in rates:
        if rate['code'] == currency:
            # return str(rate['rate'])
            return f'1 bitcoin in {currency}={str(rate["rate"])}'
    return 'N/A'


@app.route('/customers')
def get_customers():
    records = execute_query('SELECT FirstName, LastName FROM Customers')
    return str(records)


@app.route('/unique_names')
def get_unique_names():
    records = execute_query('SELECT COUNT(DISTINCT FirstName) as count FROM Customers')
    return str(records)


@app.route('/tracks_count')
def get_tracks_count():
    records = execute_query('SELECT COUNT(*) as count FROM Tracks')
    return str(records)


@app.route('/gross_turnover')
def get_gross_turnover():
    records = execute_query('SELECT  SUM(UnitPrice * Quantity) FROM Invoice_Items;')
    return str(records)


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8003)
