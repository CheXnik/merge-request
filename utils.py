import datetime
import os
import random
import sqlite3


def execute_query(query):
    db_path = os.path.join(os.getcwd(), 'chinook.db')
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()
    cur.execute(query)
    records = cur.fetchall()
    return records


def parse_length(request, default=10):
    length = request.args.get('length', str(default))

    if not length.isnumeric():
        raise ValueError("VALUE ERROR: int")

    length = int(length)

    if not 3 < length < 100:
        raise ValueError("RANGE ERROR: [3..10]")

    return length


def gen_password(length, charss):
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    if charss == '1':
        result = ''.join([
            str(random.choice(chars))
            for _ in range(length)
        ])
    else:
        result = ''.join([
            str(random.randint(0, 9))
            for _ in range(length)
        ])
    return result


def generate_current_time():
    return datetime.datetime.now()
